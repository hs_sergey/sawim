20
"Улыбка" :-), :), =), =)), :-)), :)), +) *SMILE*, *smile*
"Жаль" :-(, :(, =(, :'(, +(, =(, :-((, :((, +((, =((
"Подмигивать" ;-), ;), ^_~
"Привет" *HI*, *PREVED*, *PRIVET*, *HELLO*, *hi*, *hello*, *Hi*
"Язык" :-P, :P, :-p, :p, +P, =P, +p, =p, :-b, :b, +b, =b
"Бее" :-\, :-/, =/, =\, *BEEE*
"Бугага" :-D, :D, +D, =D, :-)))), :)))), =)))), *BIGGRIN*, *LOL*, *big_smile*, :biggrin:, :BIGGRIN:, *BIG_SMILE*, *BIG SMILE*, *big smile*, *smekh*, *SMEKH*, *laugh*, *LAUGH*, *LOL*, *lol*
"Шутка" *JOKINGLY*, *jokingly*, :o)
"Смущен" :-[, :[, (e), (E), :'), :'-), ='), ;'>, ;-.
"Звезда в шоке" =-O, =O, =-o, O_O, O_o, o_O, *MEGA_SHOK*, *MEGA SHOCK*
"Плач" :'(,:-'(,:'-(
"Класс" *THUMBS UP*, *thumbs up*, *THUMBS_UP*, *thumbs_up*, *THUMBSUP*, *thumbsup*, *GOOD*, *good*
"Без понятий" *DONT_KNOW*, *DONT KNOW*, *UNKNOWN*, *dont_know*
"Пацталом" *ROFL*, =))))), =)))))), *rofl*
"Дьявол" ]:->, >:-(, }:->, ]:>, }:>, >:-], >:], (6), *DIABLO*, *DEVIL*, *MAD*
"Поцелуй" *KISSING*, *kissing*, *GIRLKISS*, *GIRL KISS*, *GIRL_KISS*, *GIRL_KISSED*, *GIRL KISSED*, *GIRL_KISSING*, *GIRL KISSING*
"Фуу" :-!, :!, :-~, ;-~, :(~, +(~, =(~, *SICK*, *sick*
"YAHOO" *YAHOO*,*yahoo*
"Ууууууу" %), %-), :-$, :$, *WACKO*
"Дурак" :-|, :|, =|, *ERMM*, *ermm*